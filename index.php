<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CPC | Code Day Directory</title>
    <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>

</head>

<body class="bg-slate-100">
    <div class="py-5 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <!-- We've used 3xl here, but feel free to try other max-widths based on your needs -->
        <div class="max-w-3xl mx-auto">
            <!-- This example requires Tailwind CSS v2.0+ -->
            <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
                <?php $dirs = array_filter(glob('*'), 'is_dir');
                    foreach($dirs as &$value){
                        echo ('<div
                        class="relative rounded-lg border border-gray-300 bg-white px-6 py-5 shadow-sm flex items-center space-x-3 hover:border-gray-400 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                        <div class="flex-shrink-0">
                            <img class="h-10 w-10 rounded-full"
                                src=""
                                alt="">
                        </div>
                        <div class="flex-1 min-w-0">
                            <a href="./'.$value.'" class="focus:outline-none">
                                <span class="absolute inset-0" aria-hidden="true"></span>
                                <p class="text-sm font-medium text-gray-900">'.$value.'</p>
                            
                                </a>
                            </div>
                        </div>');
                    }
                    ?>

                <!-- More people... -->
            </div>
        </div>
    </div>
</body>

</html>
