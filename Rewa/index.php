
<!DOCTYPE html>

<html lang="en"><script>(function() {window['_docs_force_html_by_ext'] = '${chrome.runtime.id}';})();</script><head></head><body style="background-color:powderblue;">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to the Universe</title>
    
    <style>
        h1 {text-align:center;color:white;}
        h2 {font-family:verdana; font-size:20px; color:white}
        p {font-style:italic;}
        img {max-width:320px; height:auto}
    </style>

    <h1> Welcome to the Universe! </h1>

    <br>

    <h2>
        Lots of cool things happen here.
    </h2>

    <br>

    <p> Black holes </p>

    <img src="assets/images/blackhole.jpeg" alt="Black hole">

    <p> Supernovas </p>

    <img src="assets/images/supernova.png" alt="Supernova">

    <p> Exoplanetary systems </p>

    <img src="assets/images/exoplanets.jpeg" alt="Exoplanets">

    <p> Merging galaxies </p>

    <img src="assets/images/collision.jpeg" alt="Merging galaxies">

    <p> Sentient organisms </p>

    <img src="assets/images/whales.jpeg" alt="Whales">

    <p> Friends </p>

    <img src="assets/images/coders2.JPG" alt="Friends coding at STEM Center">

</body></html>
